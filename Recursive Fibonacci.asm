# For part 2 of Procedures and Stack Management
# Purpose of this program: Write a program which includes a
# recursive procedure to print the Fibonacci sequence.

main:
  top:
	la $a0, prompt  # ask user to enter length of sequence
	li $v0, 4
	syscall
	li $v0, 5  # get integer input from user
	syscall
	move $t0, $v0  # store user input (length of sequence) in $t0

	# if length < 0, print error message and jump to top
	bge $t0, 0, branch1_main # jump over error statement
	la $a0, error
	li $v0, 4
	syscall
	j top

  branch1_main:
	# print first two numbers in sequence (1 and 1) unless length < 2
	la $a0, output
	li $v0, 4
	syscall
	beqz, $t0, exit  # if length is 0, don't print a sequence

	la $a0, 1
	li $v0, 1
	syscall
	beq $t0, 1, exit  # if length is 1, terminate sequence here

	la $a0, ws  # whitespace
	li $v0, 4
	syscall
	la $a0, 1
	li $v0, 1
	syscall
	beq $t0, 2, exit  # if length is 2, terminate sequence here

	# call fibonacci for each number from 2 to (length - 1)
	li $t1, 2  # $t1 will be loop counter
  printLoop:
	beq $t0, $t1, exit  # stop looping when loop counter == length
	move $a0, $t1  # load argument for fibonacci to use
	jal fibonacci

	# when fibonacci returns, print its output
	la $a0, ws
	li $v0, 4
	syscall
	move $a0, $s0
	li $v0, 1
	syscall
	addu $t1, $t1, 1
	j printLoop

  exit:
	li $v0, 10  # exit program
	syscall


fibonacci:
## BASE CASES ##
	# base case 1: if input is 0, return 1
	bnez $a0, branch1_fibonacci
	li $s0, 1
	jr $ra

	# base case 2: if input is 1, 2, or 3, return input
  branch1_fibonacci:
	bgt $a0, 3, branch2_fibonacci
	move $s0, $a0
	jr $ra

## RECURSION IF BASE CASES FAIL ##
  branch2_fibonacci:
	# push $a0 and $ra onto the stack
	subu $sp, $sp, 8
	sw $a0, ($sp)
	sw $ra, 4($sp)

	# call fibonacci using (input - 1)
	subu $a0, $a0, 1
	jal fibonacci

	# pop $a0 from stack and put result from fibonacci in its place
	lw $a0, ($sp)
	sw $s0, ($sp)

	# call fibonacci using (input - 2)
	subu $a0, $a0, 2
	jal fibonacci

	# pop $s0 from stack and add to result returned from last call
	lw $s1, ($sp)
	addu $s0, $s0, $s1

	# pop $ra from stack and return from method
	lw $ra, 4($sp)
	addu $sp, $sp, 8
	jr $ra


.data
	error: .asciiz "You cannot enter a negative number! Try again.\n\n\n"
	output: .asciiz "\nHere is the sequence:\n"
	prompt: .asciiz "Enter a number to produce a Fibonacci sequence of that length: "
	ws: .asciiz " "  # add whitespace between each number printed