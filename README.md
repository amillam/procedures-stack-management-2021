**How to run *Recursive Fibonacci.asm*:**

1. Ensure the computer has [QtSpim](https://sourceforge.net/projects/spimsimulator/) installed. If it does not, install QtSpim from the given link.

2. Open QtSpim and navigate to *File > Load File*. Navigate to *Recursive Fibonacci.asm* and open the file.

3. Click "Run/Continue", located in the *Simulator* tab or on the toolbar below it.

4. Follow the instructions on the console. If the console is not visible, go to the *Window* tab and ensure "Console" is checked.

*Note: see **Downloads** panel at left for sample program output and a flow chart describing what the code does.*